var jwtSecret = require('./jwtConfig');
var bcrypt = require('bcrypt');
var knex = require("../config/knex");

const BCRYPT_SALT_ROUNDS = 12;

const passport = require('passport');
localStrategy = require('passport-local').Strategy;
JWTstrategy = require('passport-jwt').Strategy;
ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(
  'register',
  new localStrategy(
      {
          usernameField: 'email',
          passwordField: 'password',
          session: false
      },
      (username, password, done)=>{    
        try{
            bcrypt.hash(password, BCRYPT_SALT_ROUNDS).then(hashedPassword=>{
                knex('users')
                .insert({
                    email: username,
                    password: hashedPassword
                })
                .then(function(created){
                    return done(null, created[0]);
                })
            });
        }
        catch(err){
            done(err);
        }
      },
  ),  
);

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            session: false
        },
        (username, password, done)=>{
            try{
                knex
                .select('id', 'email', 'password')
                .from('users')
                .where({
                    email: username
                })
                .then(function(rows){
                    if(rows.length > 0){
                        bcrypt.compare(password, rows[0].password).then(response=>{
                            if (response!=true){
                                return done(null, false, {message: 'password do not match'});
                            }
                            else{
                                return done(null, rows[0]);
                            }
                        });
                    }else{
                        return done(null, false);
                    }
                })
            }
            catch(err){
                done(err);
            }
        },
    ),
);

const opts = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: jwtSecret.secret
};
passport.use(
    'jwt',
    new JWTstrategy(opts, (jwt_payload, done)=>{
        try{
            var query = knex
                .select('id', 'email')
                .from('users')
                .where({
                    email: jwt_payload.id
                });
            query
            .then(function(rows){
                if(rows.length > 0){
                    done(null, rows[0]);
                }else{
                    done(null, false);
                }
            })
        }
        catch(err){
            done(err)
        }
    }),
);