var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');
var passport = require('passport');

var app = express();
app.use(cors());

require('./config/passport');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(passport.initialize());

app.use(express.static(__dirname+'/uploads'));
app.use(express.static(__dirname+'/images'));

var port = 3001;
var users = require("./routing/users");
app.use('/api', users);

app.listen(port, function(){
    console.log('listening on port '+port);
});