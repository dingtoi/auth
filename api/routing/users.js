var express = require("express");
var knex = require("../config/knex");
var router = express.Router();
var jwt = require('jsonwebtoken');
var jwtConfig = require('../config/jwtConfig');
var multer  = require('multer');
var path = require('path');
var fs = require('fs');
var passport = require('passport');
var bcrypt = require('bcrypt');


router.post('/user/cart/sync', function(req, res){
    var email = req.body.email;
    var prev_email = req.body.prev_email;

    knex('users')
    .select('id')
    .where('email', prev_email)
    .asCallback(function(err1, prev_users){
        const prev_id = prev_users[0].id;
        if(err1) return res.status(500).json();
        knex('users')
        .select('id')
        .where('email', email)
        .asCallback(function(err2, users){
            const id = users[0].id;
            if(err2) return res.status(500).json();
            knex('carts')
            .select('id', 'device_id')
            .where('user_id', prev_id)
            .asCallback(function(err3, prevCarts){
                if(err3) return res.status(500).json();
                knex('carts')
                .select('id', 'device_id')
                .where('user_id', id)
                .asCallback(function(err4, carts){
                    if(err4) return res.status(500).json();
                    var updateCarts = [];
                    var removeCarts = [];
                    for(var i = 0; i < prevCarts.length; i++){
                        var prevCart = prevCarts[i];
                        var flag = false;
                        for(var j = 0; j < carts.length; j++){
                            var cart = carts[j];
                            if(cart.device_id == prevCart.device_id){
                                flag = true;
                                break;
                            }
                        }
                        if(!flag)
                            updateCarts.push(prevCart.id);
                        else
                            removeCarts.push(prevCart.id);
                    }
                    if(removeCarts.length > 0){
                        var where = '';
                        for(var i = 0; i < removeCarts.length; i++){
                            if(i == 0)
                                where += 'id = '+removeCarts[i];
                            else
                                where += ' OR id = '+removeCarts[i];
                        }
                        knex.raw(
                            `DELETE FROM carts
                            WHERE `+where
                        ).then(() => {})
                    }
                    if(updateCarts.length > 0){
                        var where = '';
                        for(var i = 0; i < updateCarts.length; i++){
                            if(i == 0)
                                where += 'id = '+updateCarts[i];
                            else
                                where += ' OR id = '+updateCarts[i];
                        }
                        knex.raw(
                            `UPDATE carts
                            SET user_id = `+id+`
                            WHERE `+where
                        ).then(() => {
                            res.json({status: 200});    
                        })
                        .catch(() => {
                            res.status(500).json();
                        })
                    }else{
                        res.status(200).json({status: 200});
                    }
                })
            })
        })
    })
});

// router.post('/user/transaction/list', function(req, res){
//     var email = req.body.email;
    
//     knex('users').select('id').where({'email': email})
//     .then(function(rows){
//         if(rows.length > 0){
//             var user_id = rows[0].id;
//             return knex.raw(
//                 `
//                 SELECT ts.number AS number
//                 , ts.device_id AS device_id
//                 , ts.exchange_device_id AS exchange_device_id
//                 , ts.user_id AS user_id, ts.order_id AS order_id
//                 , ts.price AS price
//                 , ts.type AS type
//                 ,ts.status AS status
//                 ,strftime('%d/%m/%Y', ts.created_at) AS created_at
//                 ,devices.device_name AS device_name,
//                 orders.number as order_number,
//                 exchange_devices.device_name AS exchange_device_name
//                 FROM transactions as ts
//                 INNER JOIN (
//                     SELECT devices.id, imeis.device_name
//                     FROM devices
//                     INNER JOIN (
//                         SELECT imeis.imei, models.name AS device_name
//                         FROM imeis
//                         INNER JOIN models ON models.id = imeis.model_id
//                     ) AS imeis ON imeis.imei = devices.imei
//                 ) AS devices ON devices.id = ts.device_id
//                 INNER JOIN orders ON orders.id = ts.order_id
//                 LEFT OUTER JOIN (
//                     SELECT devices.id, imeis.device_name
//                     FROM devices
//                     INNER JOIN (
//                         SELECT imeis.imei, models.name AS device_name
//                         FROM imeis
//                         INNER JOIN models ON models.id = imeis.model_id
//                     ) AS imeis ON imeis.imei = devices.imei
//                 ) AS exchange_devices ON exchange_devices.id = ts.exchange_device_id
//                 WHERE ts.user_id = `+user_id+`
//                 ORDER BY ts.created_at DESC
//                 `
//             )
//         }else{
//             res.json({status: 500});
//         }
//     })
//     .then(function(list){
//         res.json({status: 200, data: list});
//     })
//     .catch(function(error){
//         res.status(500).json();
//     })
// });
// router.post('/user/order/list', function(req, res){
//     var email = req.body.email;
    
//     knex('users').select('id').where({'email': email})
//     .then(function(rows){
//         if(rows.length > 0){
//             var user_id = rows[0].id;
//             return knex.raw(
//                 `
//                 SELECT od.id as id,
//                 od.number as number,
//                 od.user_id as user_id,
//                 od.total as total,
//                 strftime('%d/%m/%Y', od.created_at) AS created_at
//                 FROM orders od
//                 WHERE od.user_id = `+user_id+`
//                 ORDER BY od.created_at DESC
//                 `
//             )
//         }else{
//             res.json({status: 500});
//         }
//     })
//     .then(function(list){
//         res.json({status: 200, data: list});
//     })
//     .catch(function(error){
//         res.status(500).json();
//     })
// });
router.post('/user/checkPassword', function(req, res){
    var email = req.body.email;
    var password = req.body.password;
    knex('users').select('id', 'password').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            bcrypt.compare(password, rows[0].password).then(response=>{
                if (response!=true){
                    return res.json({status: 500, data: 1});
                }
                else{
                    return res.json({status: 200, data: 0});
                }
            });
        }
        else
            res.json({status: 500, data: 1});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/changePassword', function(req, res){
    var email = req.body.email;
    var password = req.body.password;

    bcrypt.hash(password, 12).then(hashedPassword=>{
        knex('users')
        .update({
            password: hashedPassword
        })
        .where('email', email)
        .then(function(updated){
            if(updated)
                return res.json({status: 200, data: updated});
            else
                return res.json({status: 500});
        })
        .catch(function(error){
            return res.status(500).json();
        })
    });
});

router.post('/user/detail', function(req, res){
    var email = req.body.email;
    knex('users').select('id', 'address1', 'address2', 'postal_code', 'city', 'country')
    .select('billing_name')
    .where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.status(500).json();
        }
    })
    .catch(function(error){
        res.status(500).json();
    })

});

router.post('/user/edit', function(req, res){
    var email = req.body.email;
    var billing_name = req.body.billing_name;
    var country = req.body.country;
    var city = req.body.city;
    var address1 = req.body.address1;
    var address2 = req.body.address2 ? req.body.address2 : null;
    var postal_code = req.body.postal_code;

    knex('users')
    .update({
        billing_name: billing_name,
        country: country,
        city: city,
        address1: address1,
        address2: address2,
        postal_code: postal_code
    })
    .where('email', email)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })

});

router.post('/user/login/admin', function(req, res, next){

    passport.authenticate('login', (err, user, info) => {
        if(err) res.json({status: 400});
        else{
            if(user){
                if(user.email == 'admin@swap-ez.com'){
                    const token = jwt.sign({id: user.email}, jwtConfig.secret);
                    res.json({data: {token: token, user: user}, status: 200});
                }else{
                    res.json({status: 400});        
                }
            }else
            res.json({status: 400});
        }
    })(req, res, next);
});

router.post('/user/login', function(req, res, next){
    //var email = req.body.email;
    //var password = req.body.password;

    passport.authenticate('login', (err, user, info) => {
        if(err) res.json({status: 400});
        else{
            if(user){
                const token = jwt.sign({id: user.email}, jwtConfig.secret);
                res.json({data: {token: token, user: user}, status: 200});
            }else
            res.json({status: 400});
        }
    })(req, res, next);

    /*var token = jwt.sign({email: email}, 'shhhh', function(err, token){
        var query = knex
                    .select('id', 'email')
                    .from('users')
                    .where({
                        email: email,
                        password: password
                    });
        query
        .then(function(rows){
            if(rows.length > 0){
                rows[0].token = token;
                res.json({data: rows[0], status: 200});
            }else{
                res.json({status: 400});
            }
        })
        .catch(function(error){
            res.status(500).json();
        })
    });*/
});

router.post('/user/login/anonymous', function(req, res){
    var ip = req.body.ip;
    knex('users').select('id').where({'email': ip})
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: '', status: 200});
        }else{
            knex('users')
            .insert({email: ip, password: 'anonymous'})
            .then(function(created){
                knex('users').select('id').where({'email': ip})
                .then(function(rows){
                    if(rows.length > 0){
                        res.json({data: '', status: 200});
                    }else
                        res.status(500);
                })
                .catch(function(error){
                    res.status(500).json();
                })
            })
            .catch(function(error){
                res.status(500).json();
            })
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/user/exists', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0)
            res.json({status: 200, data: 0});
        else
            res.json({status: 200, data: 1});
    })
    .catch(function(error){
        res.status(500).json();
    })
});

router.post('/user/register', function(req, res, next){
    //var email = req.body.email;
    //var password = req.body.password;

    passport.authenticate('register', (err, user, info) =>{
        if(err){
            res.json({status: 500});
        }else
            res.json({status: 200, data: user});
    })(req, res, next);

    /*knex('users')
    .insert({
        email: email,
        password: password
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })*/
});
router.post('/user/avatar', function(req, res){
    var email = req.body.email;
    var query = knex('users')
                .select('email', 'image')
                .where('email', email)
    query.then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/imageUpload', function(req, res, next){
    var storage = multer.diskStorage({
        destination: function(req, file, cb){
            var dest = path.resolve(__dirname, '..', 'uploads', 'users');
            cb(null, dest);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
        }
    });
    
    var upload = multer({ storage: storage });
    upload.single('photo')(req, res, function(err){
        if (err instanceof multer.MulterError) {
            res.json({status: 400});
        } else if (err) {
            console.log("dsa", err)
            res.json({status: 400});
        }
        var email = req.body.email;
        var filename = req.file.filename;

        knex('users')
        .update({
            image: filename,
        })
        .where({'email':email})
        .then(function(updated){
            res.json({status: 200});
        })
        .catch(function(error){
            res.status(500).json();
        })
    })
});
router.delete('/user/delete/image', function(req, res){
    var email = req.body.email;
    var query = knex('users')
                .select('email', 'image')
                .where('email', email)
    query.then(function(rows){
        if(rows.length > 0){
            knex('users')
            .where('email', email)
            .update('image', '')
            .then(function(deleted){
                var file = path.resolve(__dirname, '..', 'uploads', 'users', rows[0].image);
                fs.unlink(file, (err) => {
                    if (err) {
                        res.status(500).json();
                        return;
                    }
                    
                })
                res.json({status: 200, data: deleted});
            })
            .catch(function(error){
                res.status(500).json();
            });
        }else{
            res.json({status: 500});
        }
    })
});
router.get('/user/auth', function(req, res, next){
    passport.authenticate('jwt', {session: false}, (err, user) => {
        res.json({status: 200});
    })(req, res, next);
});
module.exports = router;